package admin

import (
	"github.com/silenceper/xingyun"
)

func GetViewData(ctx *xingyun.Context) map[string]interface{} {
	noticeStr := ctx.GetFlash().Notice
	return map[string]interface{}{
		"noticeStr": noticeStr,
	}
}
