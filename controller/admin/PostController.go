package admin

import (
	"strconv"
	"strings"

	"../../service"
	"../../util"
	"github.com/silenceper/xingyun"
)

//显示文章列表
func PostListHandler(ctx *xingyun.Context) {
	var limit int64 = 10
	query := map[string]interface{}{}
	page, ok := ctx.Params["page"]
	if !ok {
		page = "1"
	}
	pageNum, _ := util.StringToInt64(page)

	//获取总页数
	pageSize, err := service.GetPostPageSize(query, limit)
	if err != nil {
		ctx.Logger.Errorf("总页数获取失败", err)
		ctx.WriteString("总页数获取失败")
		ctx.NotFound()
		return
	}
	if pageNum < 1 {
		pageNum = 1
	}

	if pageNum > pageSize {
		pageNum = pageSize
	}

	offset := (pageNum - 1) * limit

	postData, err := service.GetPostList(query, offset, limit)
	if err != nil {
		ctx.Logger.Errorf("文章列表获取失败", err)
		ctx.WriteString("文章列表获取失败")
		ctx.NotFound()
		return
	}

	data := GetViewData(ctx)
	data["postData"] = postData
	data["currentPage"] = pageNum
	data["prePageNum"] = pageNum - 1
	data["nextPageNum"] = pageNum + 1
	data["pageSize"] = pageSize
	ctx.Render("admin/post_list", data)
}

//显示添加文章
func PostNewHandler(ctx *xingyun.Context) {
	data := GetViewData(ctx)
	//获取栏目列表
	cates, err := service.GetAllCategory()
	if err != nil {
		ctx.Logger.Errorf("栏目获取失败", err)
		ctx.WriteString("栏目获取失败")
		ctx.NotFound()
		return
	}
	if len(cates) == 0 {
		ctx.SetFlashNotice("请先添加指定栏目")
		ctx.Redirect("/admin")
		return
	}
	data["cates"] = cates
	ctx.Render("admin/post_new", data)

}

//处理添加文章
func PostDoNewHandler(ctx *xingyun.Context) {
	title := ctx.Params["title"]
	content := ctx.Params["text"]
	tags := ctx.Params["tags"]
	pubdateStr := ctx.Params["date"]
	status := ctx.Params["status"]
	category := ctx.Params["category"]

	title = strings.TrimSpace(title)
	content = strings.TrimSpace(content)

	if title == "" || content == "" {
		ctx.SetFlashNotice("标题和内容必填")
		ctx.Redirect("/admin/post/new")
		return
	}
	catId, err := strconv.Atoi(category)
	if err != nil {
		ctx.SetFlashNotice("没有想要的栏目")
		ctx.Redirect("/admin/post/new")
		return
	}

	//将日期转为时间戳的形式
	var pubdte int64
	if pubdateStr != "" {
		//jquery ui 没有秒 自己补上 蛋疼
		pubdateStr = pubdateStr + ":00"
		var err error
		pubdte, err = util.StringToUnix(pubdateStr)
		if err != nil {
			ctx.Logger.Errorf("时间格式转换出错", err)
			ctx.SetFlashNotice("时间字符串无法转为时间戳")
			ctx.Redirect("/admin/post/new")
			return
		}
	} else {
		pubdte = util.Now()
	}
	//状态
	statusNUm, _ := strconv.Atoi(status)
	err = service.WritePost(title, content, tags, pubdte, int64(catId), int32(statusNUm), 0)
	if err != nil {
		ctx.Logger.Errorf("添加文章出错", err)
		ctx.SetFlashNotice("添加文章出错")
		ctx.Redirect("/admin/post/new")
		return
	}
	ctx.SetFlashNotice("文章添加成功")
	ctx.Redirect("/admin/post")
}

//显示修改文章
func PostUpdateHandler(ctx *xingyun.Context) {
	idStr := ctx.Params["id"]
	idNum, _ := util.StringToInt64(idStr)
	postData, err := service.GetPostDetail(idNum)
	if err != nil {
		ctx.Logger.Errorf("获取文章出错", err)
		ctx.SetFlashNotice("获取文章出错")
		ctx.Redirect("/admin/post/list")
		return
	}

	//获取栏目列表
	cates, err := service.GetAllCategory()
	if err != nil {
		ctx.Logger.Errorf("栏目获取失败", err)
		ctx.WriteString("栏目获取失败")
		ctx.NotFound()
		return
	}

	data := GetViewData(ctx)
	data["postData"] = postData
	data["cates"] = cates
	ctx.Render("admin/post_update", data)
}

//处理修改文章
func PostDoUpdateHandler(ctx *xingyun.Context) {
	idStr := ctx.Params["id"]
	title := ctx.Params["title"]
	content := ctx.Params["text"]
	tags := ctx.Params["tags"]
	pubdateStr := ctx.Params["date"]
	status := ctx.Params["status"]
	category := ctx.Params["category"]

	title = strings.TrimSpace(title)
	content = strings.TrimSpace(content)

	if idStr == "" {
		ctx.SetFlashNotice("参数不正确")
		ctx.Redirect("/admin/post")
		return
	}
	id, err := util.StringToInt64(idStr)
	if err != nil {
		ctx.SetFlashNotice("id转换失败")
		ctx.Redirect("/admin/post")
		return
	}

	if title == "" || content == "" {
		ctx.SetFlashNotice("标题和内容必填")
		ctx.Redirect("/admin/post/new")
		return
	}
	catId, err := strconv.Atoi(category)
	if err != nil {
		ctx.SetFlashNotice("没有想要的栏目")
		ctx.Redirect("/admin/post/new")
		return
	}

	//将日期转为时间戳的形式
	var pubdte int64
	if pubdateStr != "" {
		//jquery ui 没有秒 自己补上 蛋疼
		pubdateStr = pubdateStr + ":00"
		var err error
		pubdte, err = util.StringToUnix(pubdateStr)
		if err != nil {
			ctx.Logger.Errorf("时间格式转换出错", err)
			ctx.SetFlashNotice("时间字符串无法转为时间戳")
			ctx.Redirect("/admin/post/new")
			return
		}
	} else {
		pubdte = util.Now()
	}
	//状态
	statusNUm, _ := strconv.Atoi(status)
	err = service.WritePost(title, content, tags, pubdte, int64(catId), int32(statusNUm), id)
	if err != nil {
		ctx.Logger.Errorf("修改文章出错", err)
		ctx.SetFlashNotice("修改文章出错")
		ctx.Redirect("/admin/post/update/" + idStr)
		return
	}
	ctx.SetFlashNotice("文章修改成功")
	ctx.Redirect("/admin/post/update/" + idStr)
}
