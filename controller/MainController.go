package controller

import (
	"../model"
	"../service"
	"../util"
	"github.com/silenceper/xingyun"
)

//首页
func IndexHandler(ctx *xingyun.Context) {
	var limit int64 = 10
	query := map[string]interface{}{"status": 1}
	page, ok := ctx.Params["page"]
	if !ok {
		page = "1"
	}
	pageNum, _ := util.StringToInt64(page)

	//获取总页数
	pageSize, err := service.GetPostPageSize(query, limit)
	if err != nil {
		ctx.Logger.Errorf("总页数获取失败", err)
		ctx.WriteString("总页数获取失败")
		ctx.NotFound()
		return
	}
	if pageNum < 1 {
		pageNum = 1
	}

	if pageNum > pageSize {
		pageNum = pageSize
	}

	offset := (pageNum - 1) * limit

	postData, err := service.GetPostList(query, offset, limit)
	if err != nil {
		ctx.Logger.Errorf("文章列表获取失败", err)
		ctx.WriteString("文章列表获取失败")
		ctx.NotFound()
		return
	}

	data := GetViewData(ctx)
	data["postData"] = postData
	data["currentPage"] = pageNum
	data["prePageNum"] = pageNum - 1
	data["nextPageNum"] = pageNum + 1
	data["pageSize"] = pageSize
	if pageNum > 1 {
		data["title"] = "首页 - 第" + page + "页 "
	} else {
		data["title"] = "首页"
	}

	ctx.Render("index", data)
}

//详情
func DetailHandler(ctx *xingyun.Context) {
	idStr, ok := ctx.Params["id"]
	if !ok {
		ctx.NotFound()
		return
	}

	id, _ := util.StringToInt64(idStr)
	postData, err := service.GetPostDetail(id)
	if err != nil {
		ctx.Logger.Errorf("信息获取失败", err)
		ctx.NotFound()
		return
	}
	//判断是否允许公开
	post := postData["post"]
	obj, _ := post.(model.Post)
	if obj.Status != model.STATUS_PUBLIC {
		ctx.Logger.Errorf("无权限查看", err)
		ctx.NotFound()
		return
	}
	//获取上下页文章信息
	preId, err := model.GetPrePostId(id)
	if err != nil {
		ctx.Logger.Errorf("preId信息获取失败", err)
		ctx.NotFound()
		return
	}
	nextId, err := model.GetNextPostId(id)
	if err != nil {
		ctx.Logger.Errorf("nextId获取失败", err)
		ctx.NotFound()
		return
	}

	var prePostData map[string]interface{}
	var nextPostData map[string]interface{}
	if preId != 0 {
		prePostData, err = service.GetPostDetail(preId)
		if err != nil {
			ctx.Logger.Errorf("上篇文章获取失败", err)
			ctx.NotFound()
			return
		}
	}
	if nextId != 0 {
		nextPostData, err = service.GetPostDetail(nextId)
		if err != nil {
			ctx.Logger.Errorf("下篇文章获取失败", err)
			ctx.NotFound()
			return
		}
	}
	data := GetViewData(ctx)
	data["postData"] = postData
	data["prePostData"] = prePostData
	data["nextPostData"] = nextPostData
	data["title"] = obj.Title
	ctx.Render("detail", data)
}

//分类页面
func CategoryPostHandler(ctx *xingyun.Context) {
	catIdStr := ctx.Params["catId"]
	catIdNum,_ := util.StringToInt64(catIdStr)
	page, ok := ctx.Params["page"]
	if !ok {
		page = "1"
	}
	pageNum, _ := util.StringToInt64(page)
	var limit int64 = 10
	query := map[string]interface{}{"status": 1,"catId":catIdStr}

	//获取栏目信息
	category,err:=service.GetCategoryById(catIdNum)
	if err!=nil{
		ctx.WriteString("不存在该栏目分类")
		ctx.NotFound()
		return
	}
	//获取总页数
	pageSize, err := service.GetPostPageSize(query, limit)
	if err != nil {
		ctx.Logger.Errorf("总页数获取失败", err)
		ctx.WriteString("总页数获取失败")
		ctx.NotFound()
		return
	}
	if pageNum < 1 {
		pageNum = 1
	}

	if pageNum > pageSize {
		pageNum = pageSize
	}

	offset := (pageNum - 1) * limit

	postData, err := service.GetPostList(query, offset, limit)
	if err != nil {
		ctx.Logger.Errorf("文章列表获取失败", err)
		ctx.WriteString("文章列表获取失败")
		ctx.NotFound()
		return
	}

	data := GetViewData(ctx)
	data["category"] = category
	data["postData"] = postData
	data["currentPage"] = pageNum
	data["prePageNum"] = pageNum - 1
	data["nextPageNum"] = pageNum + 1
	data["pageSize"] = pageSize
	if pageNum > 1 {
		data["title"] = "分类  "+category.Name+" 下的文章"+" 第" + page + "页 "
	} else {
		data["title"] = "分类  "+category.Name+" 下的文章"
	}

	ctx.Render("category", data)
}

//tag
func TagPostHandler(ctx *xingyun.Context) {
	//tag := ctx.Params["tag"]
	//pageStr := ctx.Params["page"]
}
