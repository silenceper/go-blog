package controller

import(
	"github.com/silenceper/xingyun"
)

//关于界面
func AboutHandler(ctx *xingyun.Context){
	data := GetViewData(ctx)
	data["title"]="关于我"
	ctx.Render("about", data)
}