package util

import (
	"regexp"
	"strings"

	"github.com/russross/blackfriday"
)

func HtmlTostr(html string) string {
	src := string(html)

	//将HTML标签全转换成小写
	re, _ := regexp.Compile("\\<[\\S\\s]+?\\>")
	src = re.ReplaceAllStringFunc(src, strings.ToLower)

	//去除STYLE
	re, _ = regexp.Compile("\\<style[\\S\\s]+?\\</style\\>")
	src = re.ReplaceAllString(src, "")

	//去除SCRIPT
	re, _ = regexp.Compile("\\<script[\\S\\s]+?\\</script\\>")
	src = re.ReplaceAllString(src, "")

	//去除所有尖括号内的HTML代码，并换成换行符
	re, _ = regexp.Compile("\\<[\\S\\s]+?\\>")
	src = re.ReplaceAllString(src, "\n")

	//去除连续的换行符
	re, _ = regexp.Compile("\\s{2,}")
	src = re.ReplaceAllString(src, "\n")

	return strings.TrimSpace(src)
}

//解析markdown语法
func MarkdownToHtml(text string) string {
	return string(blackfriday.MarkdownCommon([]byte(text)))
}

//获取summary
func GetSummary(text string) string {
	return strings.Split(text, "<!--more-->")[0]
}

//去掉more
func ReplaceMore(text string) string {
	return strings.Replace(text, "<!--more-->", "", -1)
}
