package util

import (
	"strconv"
)

func StringToInt64(str string)(int64,error){
	num,err:=strconv.Atoi(str)
	if err!=nil{
		return 0,err
	}
	return int64(num),nil
}