package main

import (
	"log"
	"net/http"
	"os"

	"./controller"
	"./controller/admin"
	"./model"
	"./util"
	"github.com/silenceper/xingyun"
)

func init() {
	//设置一下时区
	model.InitDb()
	model.Dbmap.TraceOn("[gorp]", log.New(os.Stdout, "go-blog:", log.Lmicroseconds))
}

func main() {
	cfg := &xingyun.Config{
		StaticHost: "/",
		ViewPath:   "view",
		Layout:     "layout",
	}
	server := xingyun.NewServer(cfg)
	logger := server.Logger()

	//pipe setting
	pipe := server.NewPipe("normal", xingyun.PipeHandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		cfg.ViewPath = "view"
		cfg.Layout = "layout"
		ctx := xingyun.GetContext(r)
		ctx.View.FuncMap["htmlTostr"] = util.HtmlTostr
		ctx.View.FuncMap["markdownToHtml"] = util.MarkdownToHtml
		ctx.View.FuncMap["getSummary"] = util.GetSummary
		ctx.View.FuncMap["replaceMore"] = util.ReplaceMore
		ctx.View.FuncMap["unixToFormatString"] = util.UnixToFormatString
		next(w, r)
	}))

	//route mapping
	server.Get("/", pipe.Wrap(controller.IndexHandler))
	server.Get("/about", pipe.Wrap(controller.AboutHandler))
	server.Get("/page/{page:[0-9]+}", pipe.Wrap(controller.IndexHandler))
	server.Get("/archives/{id:[0-9]+}", pipe.Wrap(controller.DetailHandler))
	//兼容之前blog链接
	server.Get("/archives/{id:[0-9]+}.html", pipe.Wrap(controller.DetailHandler))
	
	server.Get("/category/{catId:[0-9]+}", pipe.Wrap(controller.CategoryPostHandler))
	server.Get("/category/{catId:[0-9]+}/{page:[0-9]+}", pipe.Wrap(controller.CategoryPostHandler))
	server.Get("/tag/{tag:[^/]+}", pipe.Wrap(controller.TagPostHandler))
	server.Get("/tag/{tag:[^/]+}/{page:[0-9]+}", pipe.Wrap(controller.TagPostHandler))

	//验证登入权限
	AuthPipe := server.NewPipe("auth", xingyun.PipeHandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		cfg.Layout = "admin/layout"
		ctx := xingyun.GetContext(r)
		loginState := ctx.GetSession("isLogin")
		//fmt.Println(loginState)
		if string(loginState) != "yes" {
			//跳转至登入页面
			ctx.Redirect("/admin/login")
			return
		}
		ctx.View.FuncMap["unixToFormatString"] = util.UnixToFormatString
		next(w, r)
	}))

	//admin route
	//登入
	server.Get("/admin/login", admin.LoginHandler)
	server.Post("/admin/login", admin.DoLoginHandler)

	//文章列表
	server.Get("/admin/post", AuthPipe.Wrap(admin.PostListHandler))

	//新增文章
	server.Get("/admin/post/new", AuthPipe.Wrap(admin.PostNewHandler))
	server.Post("/admin/post/new", AuthPipe.Wrap(admin.PostDoNewHandler))

	//更新文章
	server.Get("/admin/post/update/{id:[0-9]+}", AuthPipe.Wrap(admin.PostUpdateHandler))
	server.Post("/admin/post/update/{id:[0-9]+}", AuthPipe.Wrap(admin.PostDoUpdateHandler))

	err := server.ListenAndServe(":8082")
	logger.Errorf("%s", err)
}
