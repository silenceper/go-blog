package model

import "time"

type Category struct {
	Id          int32  `db:"id"`
	Name        string `db:"name"`
	Description string `db:"description"`
	Utime       int64  `db:"utime"`
	Ctime       int64  `db:"ctime"`
}

func NewCategory(name, description string, utime int64) *Category {
	return &Category{
		Name:        name,
		Description: description,
		Utime:       utime,
		Ctime:       time.Now().Unix(),
	}
}

func CheckCatExistsById(id int64) bool {
	count, err := Dbmap.SelectInt("select count(*) from category where id=?", id)
	if err == nil && count == 0 {
		return false
	}
	return true
}

//获取栏目列表
func GetCategoryList() ([]Category, error) {
	var cates []Category
	_, err := Dbmap.Select(&cates, "select * from category")
	return cates, err
}

//根据id获取栏目
func GetCategoryById(id int64) (Category, error) {
	var cate Category
	err := Dbmap.SelectOne(&cate, "select * from category where id=?", id)
	return cate, err
}
