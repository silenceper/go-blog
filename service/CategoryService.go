package service

import (
	"../model"
)

//获取所有栏目
func GetAllCategory() ([]model.Category, error) {
	return model.GetCategoryList()
}

//获取栏目信息
func GetCategoryById(id int64) (model.Category, error) {
	return model.GetCategoryById(id)
}
